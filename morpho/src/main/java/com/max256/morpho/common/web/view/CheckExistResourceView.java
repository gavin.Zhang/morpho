package com.max256.morpho.common.web.view;

import java.io.File;
import java.util.Locale;

import org.springframework.web.servlet.view.InternalResourceView;

/**
 * 视图解析器 检查视图是否存在 不存在的话继续之后的视图解析执行链
 * @author fbf
 *
 */
public class CheckExistResourceView extends InternalResourceView {

	@Override
	public boolean checkResource(Locale locale) {
		File file = new File(this.getServletContext().getRealPath("/") + getUrl());
		return file.exists(); // 判断页面是否存在
	}
}